// Create an array of words
maintext=document.getElementById('main');
gameimg=document.getElementsByClassName('container');
maintext.display='block';
var animalsarr = [
	"кіт",
	"собака",
	"лисиця",
	"кролик",
	"мавпа",
	
]

var transportarr= [
  "машина",
  'літак',
  "потяг",
  "автобус"
];

var foodarr= [
  "яблуко",
  "банан",
  "груша",
  "виноград",
  "ківі",
]

var countriesarr=[
  "Україна",
  "Нідерланди",
  "Німеччина",
  "США",
  "Москальщина"
];






function game()
{
let answer = '';
let maxWrong = 6;
let mistakes = 0;
let guessed = [];
let wordStatus = null;
words=countriesarr;
function randomWord() {
  answer = words[Math.floor(Math.random() * words.length)];
}

function handleGuess(chosenLetter) {
  if (guessed.indexOf(chosenLetter) === -1)  {guessed.push(chosenLetter)} else { null};
 document.getElementById(chosenLetter).setAttribute('disabled', true);

 if (answer.indexOf(chosenLetter) >= 0) {
   guessedWord();
   checkIfGameWon();
 } else if (answer.indexOf(chosenLetter) === -1) {
   mistakes++;
   updateMistakes();
   checkIfGameLost();
   updateHangmanPicture();
 }
}

function generateButtons() {
  let buttonsHTML = 'абвгґдеєжзиіїйклмнопрстуфхцчшщьюя'.split('').map(letter =>
    `
      <button
        
        id='` + letter + `'
        onClick="handleGuess('` + letter + `')"
      >
        ` + letter + `
      </button>
    `).join('');

  document.getElementById('keyboard').innerHTML = buttonsHTML;
}



function updateHangmanPicture() {
  document.getElementById('hangmanPic').src = './images/' + mistakes + '.jpeg';
}

function checkIfGameWon() {
  if (wordStatus === answer) {
  	
    document.getElementById('keyboard').innerHTML = 'Ви перемогли!';
  }
}

function checkIfGameLost() {
  if (mistakes === maxWrong) {
    document.getElementById('wordSpotlight').innerHTML = 'Правильна відповідь: ' + answer;
    
    document.getElementById('keyboard').innerHTML = 'Ви програли!';
  }
}

function guessedWord() {
  wordStatus = answer.split('').map(letter => (guessed.indexOf(letter) >= 0 ? letter : " _ ")).join('');

  document.getElementById('wordSpotlight').innerHTML = wordStatus;
}

function updateMistakes() {
  document.getElementById('mistakes').innerHTML = mistakes;
}

function reset() {
  mistakes = 0;
  guessed = [];
  document.getElementById('hangmanPic').src = './images/0.jpeg';

  randomWord();
  guessedWord();
  updateMistakes();
  generateButtons();
}

document.getElementById('maxWrong').innerHTML = maxWrong;

randomWord();
generateButtons();
guessedWord();
}

game();

function food() {
  words=foodarr;
  document.getElementById('main').style.display='none';
  document.getElementById('container').style.display='block';
  game()
}

function transport()
{
  words=transportarr;
  document.getElementById('main').style.display='none';
  document.getElementById('container').style.display='block';
  game()
}

function animals()
{
  words=animalsarr;
  document.getElementById('main').style.display='none';
  document.getElementById('container').style.display='block';
  game()
}

function countries()
{
  words=countriesarr;
  document.getElementById('main').style.display='none';
  document.getElementById('container').style.display='block';
  game()
}